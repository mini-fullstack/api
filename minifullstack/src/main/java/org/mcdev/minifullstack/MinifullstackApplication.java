package org.mcdev.minifullstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinifullstackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinifullstackApplication.class, args);
	}

}
