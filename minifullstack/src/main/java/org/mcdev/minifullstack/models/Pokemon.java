package org.mcdev.minifullstack.models;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Pokemon {
  

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private int id;
  private int type;
  private String name;
  private String region;
  private String unlockCode;

  public int getId() {
    return id;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getUnlockCode() {
    return unlockCode;
  }

  // TODO: find better way to set unlock code
  public void setUnlockCode(String unlockCode) {
    this.unlockCode = unlockCode;
  }

  public Pokemon(int type, String name, String region, String unlockCode) {
    this.type = type;
    this.name = name;
    this.region = region;
    this.unlockCode = unlockCode;
  }
 
}
